# Minezoom Server [![build status](https://gitlab.com/minezoom/server/badges/master/build.svg)](https://gitlab.com/minezoom/server/commits/master)

This repository builds an initial Minecraft server configuration running [Paper](https://github.com/PaperMC/Paper).

## Building

A flavor of Linux is required.

```bash
./build.sh
```

If successful, a `target/server.zip` file will be generated.