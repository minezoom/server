TARGET_DIR=target
TARGET=server.zip
TMP_DIR=tmp
FILES_DIR=files

echo "Cleaning..."
rm -rf $TARGET_DIR
rm -rf $TMP_DIR
mkdir $TARGET_DIR
mkdir $TMP_DIR

echo "Preparing zip..."
cp -rf $FILES_DIR/* $TMP_DIR

echo "Generating release..."
cd $TMP_DIR && \
  zip -r ../$TARGET_DIR/$TARGET . && \
  cd ..

echo "Cleaning up..."
rm -rf $TMP_DIR

echo "Completed: $TARGET_DIR/$TARGET"