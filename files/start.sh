#!/bin/ash
PAPERCLIP_BUILD=1140
JAR=paperclip-$PAPERCLIP_BUILD.jar
URL=https://ci.destroystokyo.com/job/PaperSpigot/$PAPERCLIP_BUILD/artifact/paperclip.jar

# infinite soft restarts
while true
do
  # check if server jar is present and is valid
  if ! jar tf $JAR &> /dev/null; then
    echo "Server jar, $JAR, failed, cleaning up jars..."
    rm -f server-*.jar
    echo "Fetching server build $PAPERCLIP_BUILD from $URL..."
    wget -O $JAR -nv $URL
  fi

  java -jar \
       -Xmx2g \
       $JAR

  echo "Soft restarting Minecraft service in 5 seconds..."
  sleep 5
done
